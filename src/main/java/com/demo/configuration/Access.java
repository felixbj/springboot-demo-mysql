package com.demo.configuration;

public enum Access {
    ADMINISTER,
    PUBLISH,
    VIEWER
}
