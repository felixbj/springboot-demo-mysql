package com.demo.configuration;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.demo.dao.ProfileDao;
import com.demo.dao.RolDao;
import com.demo.dao.UserDao;
import com.demo.models.Profile;
import com.demo.models.Rol;
import com.demo.models.User;
import com.demo.security.JWTAuthorizationFilter;

@Configuration
@EnableWebSecurity
public class DemoConfiguration extends WebSecurityConfigurerAdapter {
	@Autowired
	private UserDao userDao;
	@Autowired
	private ProfileDao profileDao;
	@Autowired
	private RolDao rolDao;
	@Autowired
	private PasswordEncoder passwordEncoder;
	/**
	 * Bean 
	 **/
	@Bean
	public ModelMapper modelMapper() {
	    return new ModelMapper();
	}
	@Bean
	public PasswordEncoder encoder() {
	    return new BCryptPasswordEncoder();
	}
	/**
	 * Listener
	 **/
	@EventListener
	public void seed(ContextRefreshedEvent event) {
	    seedUserTable();
	    seedRolTable();
	}
	/**
	 * Override
	 **/
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable()
			// anade filtro de jwt
			.addFilterAfter(new JWTAuthorizationFilter(), UsernamePasswordAuthenticationFilter.class)
			// no autenticacion para esta solicitud
			.authorizeRequests().antMatchers(HttpMethod.POST, "/auth/**").permitAll()
			// todas las demas deben estar autenticadas
			.anyRequest().authenticated();
	}
	/**
	 * Seeders methods
	 **/
	private void seedRolTable() {
		Logger logger = LoggerFactory.getLogger(DemoConfiguration.class);
		if(rolDao.count() <= 2) {
			for (Access acces : Access.values()) {
				List<Rol> list = rolDao.findByAccess(acces);
				if (list == null || list.isEmpty()) {
					Rol rol = new Rol();
					Status active = Status.ACTIVE;//revisar para no dejar asi
					rol.setAccess(acces);
					rol.setName(acces.name());
					rol.setDescription(acces.name()+" DESCRIPTION");
					rol.setStatus(active);
					rol.setIsActive(true);
					rolDao.save(rol);
			        logger.info(acces.name()+" Seeded");
				}
			}
	        logger.info("Roles Seeded");
		} else {
	        logger.info("Roles Seeding Not Required");
	    }
	}
	private void seedUserTable() {
		Logger logger = LoggerFactory.getLogger(DemoConfiguration.class);
		if(userDao.count() == 0) {
			User user = new User();
			Profile profile = new Profile();
			user.setUsername("admin");
			user.setEmail("administrador@test.com");
			user.setPassword(passwordEncoder.encode("password"));
			user.setProfile(profile);
			userDao.save(user);
			profileDao.save(profile);
	        logger.info("Users Seeded");
		} else {
	        logger.info("Users Seeding Not Required");
	    }
	}
}
