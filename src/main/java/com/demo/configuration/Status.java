package com.demo.configuration;

public enum Status {
    ACTIVE,
    INACTIVE,
    LOCKED,
    REMOVED
}
