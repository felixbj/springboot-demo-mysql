package com.demo.controllers;

/*import java.sql.Date;
import java.util.List;
import java.util.stream.Collectors;*/

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//import com.demo.configuration.Access;
import com.demo.dto.SignInDto;
import com.demo.dto.SignUpDto;
import com.demo.services.AuthService;
import com.demo.services.UserService;

//import io.jsonwebtoken.Jwts;
//import io.jsonwebtoken.SignatureAlgorithm;

@RestController
@RequestMapping("auth")
public class AuthController {
	@Autowired
	private UserService userService;
	@Autowired
	private AuthService authService;
	
	@PostMapping(value="signup")//reguistro
	public void Signup(@RequestBody SignUpDto signUpDto) {
		
	}
	@PostMapping(value="signin")//iniciar sesion
	public ResponseEntity<Object> Signin(@RequestBody SignInDto signInDto) {
		if (userService.validateCredential(signInDto.getUsername(),signInDto.getPassword())) {
			return authService.getJWTToken(signInDto.getUsername());
		}
		return ResponseEntity.notFound().build();
	}
	/*@PostMapping(value="signout")//crear en usercontroller
	public void Signout() {//request req
	}*/
}
