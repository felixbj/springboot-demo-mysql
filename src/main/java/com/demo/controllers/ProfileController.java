package com.demo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.dto.ProfileDto;
import com.demo.services.ProfileService;

@RestController
@RequestMapping("profile")
public class ProfileController {
	@Autowired
	private ProfileService profileService;

	@GetMapping(value="{idProfile}")
	public ResponseEntity<ProfileDto> getOneUser(@PathVariable("idProfile") Long idProfile) {
		return profileService.getOne(idProfile);
	}
	@PutMapping(value="{idProfile}")
	public ResponseEntity<Object> editOneUser(@RequestBody ProfileDto profileDto,@PathVariable("idProfile") Long idProfile) {
		if (profileDto.getId() != null) 
			return profileService.editOne(profileDto,idProfile);
		return null;
	}
}
