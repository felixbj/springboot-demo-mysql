package com.demo.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.demo.dto.RolDto;
import com.demo.models.Rol;
import com.demo.services.RolService;

@RestController
@RequestMapping("rol")
public class RolController {
	@Autowired
	private RolService rolService;

	@PostMapping
	public ResponseEntity<Object> createOneRol(@RequestBody RolDto rolDto) {
		return rolService.createOne(rolDto);
	}
	@GetMapping(value="paginate")
	public  ResponseEntity<Page<Rol>> getPaginationUsers(@RequestParam(value="page", defaultValue="0") Optional<Integer> page){
		Integer newPage = 0;  
	    if (page.isPresent()) {
	    	newPage = page.get();
	    }
		Pageable paging = PageRequest.of(newPage, 5);
		return rolService.getPagination(paging);
	}
	@GetMapping
	public ResponseEntity<List<RolDto>> getAllRol() {
		return rolService.getAll();
	}
	@GetMapping(value="idRol")
	public ResponseEntity<RolDto> getOneRol(@PathVariable("idRol") Long idRol) {
		return rolService.getOne(idRol);
	}
	@PutMapping(value="{idRol}")
	public ResponseEntity<Object> editOneRol(@RequestBody RolDto rolDto,@PathVariable("idRol") Long idRol) {
		if( rolDto.getId() != null )
			return rolService.editOne(rolDto,idRol);
		return null;
	}
	@DeleteMapping(value="{idRol}")
	public void markDeleteOneRol(@PathVariable("idRol") Long idRol) {
		rolService.markDeleteOne(idRol);
	}
	@DeleteMapping(value="delete/{idRol}")
	public void deleteOneRol(@PathVariable("idRol") Long idRol) {
		rolService.deleteOne(idRol);
	}
}
