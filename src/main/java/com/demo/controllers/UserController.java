package com.demo.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.demo.dto.UserDto;
import com.demo.models.User;
import com.demo.services.UserService;

@RestController
@RequestMapping("user")
public class UserController {
	@Autowired
	private UserService userService;
	//Get Post Put Delete 200 400 404 500
	/**
	 * token 
	 **/
	@GetMapping(value="hello")
	public void greetingToken() {
		
	}
	/***/
	@PostMapping
	public ResponseEntity<Object> createOneUser(@RequestBody UserDto userDto) {
		return userService.createOne(userDto);
	}
	@GetMapping(value="paginate")
	public  ResponseEntity<Page<User>> getPaginationUsers(@RequestParam(value="page", defaultValue="0") Optional<Integer> page){
		Integer newPage = 0;  
	    if (page.isPresent()) {
	    	newPage = page.get();
	    }
		Pageable paging = PageRequest.of(newPage, 5);
		return userService.getPagination(paging);
	}
	@GetMapping
	public ResponseEntity<List<UserDto>> getAllUser() {
		return userService.getAll();
	}
	//@RequestMapping(value="{idUser}",method=RequestMethod.GET)
	@GetMapping(value="{idUser}")
	public ResponseEntity<UserDto> getOneUser(@PathVariable("idUser") Long idUser) {
		return userService.getOne(idUser);
	}
	@PutMapping(value="{idUser}")
	public ResponseEntity<Object> editOneUser(@RequestBody UserDto userDto,@PathVariable("idUser") Long idUser) {
		if (userDto.getId() != null) 
			return userService.editOne(userDto,idUser);
		return null;
	}
	@DeleteMapping(value="{idUser}")
	public void markDeleteOneUser(@PathVariable("idUser") Long idUser) {
		userService.markDeleteOne(idUser);
	}
	@DeleteMapping(value="delete/{idUser}")
	public void deleteOneUser(@PathVariable("idUser") Long idUser) {
		userService.deleteOne(idUser);
	}
}
