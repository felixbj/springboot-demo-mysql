package com.demo.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.configuration.Access;
import com.demo.models.Rol;

@Repository
public interface RolDao extends JpaRepository<Rol,Long>{
	Page<Rol> findAll(Pageable pegable);
	List<Rol> findByAccess(Access access);
	Rol findByName(String name);
}
