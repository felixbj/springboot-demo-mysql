package com.demo.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.models.User;

@Repository
public interface UserDao extends JpaRepository<User,Long>{
	Page<User> findAll(Pageable pegable);
	User findByEmail(String email);
	User findByUsername(String username);
}
