package com.demo.models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.demo.configuration.Access;
import com.demo.configuration.Status;
import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name="role")
public class Rol {
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	@Column(unique=true)
	private String name;
	@Column
	private String description;
	@Enumerated
	@Column
	private Access access;
	@Enumerated
	@Column
	private Status status;
	@Column(name="is_active",nullable=false)
	private Boolean isActive;
	
	@ManyToMany(mappedBy = "roles")//nombre del campo en user
	@JsonBackReference 
    private List<User> users;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Access getAccess() {
		return access;
	}
	public void setAccess(Access access) {
		this.access = access;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	public List<User> getUsers() {
		return users;
	}

}
