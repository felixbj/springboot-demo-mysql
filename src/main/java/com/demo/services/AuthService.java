package com.demo.services;

import java.sql.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.stereotype.Service;

import com.demo.configuration.Access;
import com.demo.dao.UserDao;
import com.demo.models.User;

import io.jsonwebtoken.Jwts;

@Service
public class AuthService {
	@Autowired
	private UserDao userDao;
	private final String PREFIX = "Bearer ";
	private final String SECRET = "mySecretKey";
	private final Integer EXPIRATION = 100000;
	
	public ResponseEntity<Object> getJWTToken(String username) {
		User user = userDao.findByUsername(username);
		if(user != null) {
			String token = makeJWTToken(user);
			user.setRememberToken(token);
			userDao.save(user);
			return ResponseEntity.ok(PREFIX + token);
		}else
			return ResponseEntity.notFound().build();
	}
	private String makeJWTToken(User user) {
		List<GrantedAuthority> grantedAuthorities = AuthorityUtils
				.commaSeparatedStringToAuthorityList(Access.VIEWER.name());
		String token = Jwts.builder()
				.setId(String.valueOf(user.getId()))
				.setSubject(user.getUsername())
				.claim("authorities",grantedAuthorities.stream()
					.map(GrantedAuthority::getAuthority)
					.collect(Collectors.toList()))
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + EXPIRATION))
				.signWith(io.jsonwebtoken.SignatureAlgorithm.HS512,SECRET.getBytes()).compact();		
		return token;
	}
}
