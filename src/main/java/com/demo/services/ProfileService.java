package com.demo.services;

import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.demo.dao.ProfileDao;
import com.demo.dto.ProfileDto;
import com.demo.models.Profile;

@Service
public class ProfileService {
	@Autowired
    private ModelMapper modelMapper;
	@Autowired
	private ProfileDao profileDao;
	/**
	 * Profile
	 */
	public ResponseEntity<ProfileDto> getOne(Long idUser) {
		Optional<Profile> profile = profileDao.findById(idUser);
		if(profile.isPresent())
			return ResponseEntity.ok(convertToDto(profile.get()));
		else
			return ResponseEntity.notFound().build();
	}
	public ResponseEntity<Object> editOne(ProfileDto profileDto,Long idProfile){
		Optional<Profile> profile = profileDao.findById(idProfile);
		if(profile.isPresent()) {
			Profile profileEdited = profile.get();
			if(profileDto.getFirstname() != null && !profileDto.getFirstname().isEmpty() && !profileDto.getFirstname().equals(profileEdited.getFirstname()))
				profileEdited.setFirstname(profileDto.getFirstname());
			if(profileDto.getLastname() != null && !profileDto.getLastname().isEmpty() && !profileDto.getLastname().equals(profileEdited.getLastname()))
				profileEdited.setLastname(profileDto.getLastname());
			if(profileDto.getBirthday() != null && !profileDto.getBirthday().equals(profileEdited.getBirthday()))
				profileEdited.setBirthday(profileDto.getBirthday());
			profileDao.save(profileEdited);
			return ResponseEntity.ok(convertToDto(profileEdited));
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	/**
	 * Model Mapper 
	 **/
	private ProfileDto convertToDto(Profile profile) {
		ProfileDto userDto = modelMapper.map(profile, ProfileDto.class);
	    return userDto;
	}
	
}
