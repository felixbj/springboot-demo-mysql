package com.demo.services;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.expression.ParseException;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.demo.dao.RolDao;
import com.demo.dto.RolDto;
import com.demo.models.Rol;

@Service
public class RolService {
	@Autowired
    private ModelMapper modelMapper;
	@Autowired
	private RolDao rolDao;

	public ResponseEntity<Object> createOne(RolDto rolDto) {
		Rol r = rolDao.findByName(rolDto.getName());
		if( r == null ) {
			Rol rol = convertToEntity(rolDto);
			rolDao.save(rol);
    		return ResponseEntity.created(null).build();//link
		} 
		return ResponseEntity.badRequest().build();//conflit
	}
	public ResponseEntity<List<RolDto>> getAll() {
		List<Rol> role = rolDao.findAll();
		List<RolDto> roleDto = role.stream().map(this::convertToDto).collect(Collectors.toList());
		return ResponseEntity.ok(roleDto);
	}
	public ResponseEntity<RolDto> getOne(Long idRol) {
		Optional<Rol> rol = rolDao.findById(idRol);
		if (rol.isPresent())
			return ResponseEntity.ok(convertToDto(rol.get()));
		else
			return ResponseEntity.notFound().build();
	}
	public ResponseEntity<Object> editOne(RolDto rolDto, Long idRol) {
		Optional<Rol> rol = rolDao.findById(idRol);
		if (rol.isPresent()) {
			Rol rolEdited = rol.get();
			if ( rolDto.getName() != null && !rolDto.getName().isEmpty() && !rolDto.getName().equals(rolEdited.getName()) ) {
				Rol r = rolDao.findByName(rolDto.getName());
				if (r == null)
					rolEdited.setName(rolDto.getName());
			}
			if ( rolDto.getAccess() != null && !rolDto.getAccess().name().equals(rolEdited.getAccess().name()) ) {
				rolEdited.setAccess(rolDto.getAccess());
			}
			if ( rolDto.getStatus() != null && !rolDto.getStatus().name().equals(rolEdited.getStatus().name()) ) {
				rolEdited.setStatus(rolDto.getStatus());
			}
			if ( rolDto.getDescription() != null && !rolDto.getDescription().isEmpty() && !rolDto.getDescription().equals(rolEdited.getDescription()) ) {
				rolEdited.setDescription(rolDto.getDescription());
			}
			rolDao.save(rolEdited);
			return ResponseEntity.ok(convertToDto(rolEdited));
		}
		return ResponseEntity.notFound().build();
	}
	public ResponseEntity<Object> markDeleteOne(Long idRol) {
		Optional<Rol> rol = rolDao.findById(idRol);
		if (rol.isPresent()) {
			Rol rolEdited = rol.get();
			rolEdited.setIsActive((rolEdited.getIsActive()) ? false : true);
			rolDao.save(rolEdited);
			return  ResponseEntity.ok().build();
		}
		return ResponseEntity.notFound().build();
	}
	public ResponseEntity<Object> deleteOne(Long idRol) {
		Optional<Rol> rol = rolDao.findById(idRol);
		if (rol.isPresent()) {
			if (!rol.get().getIsActive()) {
				rolDao.deleteById(idRol);
				return  ResponseEntity.ok().build();
			}
		}
		return ResponseEntity.notFound().build();
	}
	/**
	 * Paginate
	 **/
	public ResponseEntity<Page<Rol>> getPagination(Pageable paging) {
    	Page<Rol> role =  rolDao.findAll(paging);
		return ResponseEntity.ok(role);
	}
	/**
	 * Model Mapper 
	 **/
	private Rol convertToEntity(RolDto rolDto) throws ParseException {
		Rol rol = modelMapper.map(rolDto, Rol.class);
	    return rol;
	}
	private RolDto convertToDto(Rol rol) {
		RolDto rolDto = modelMapper.map(rol, RolDto.class);
	    return rolDto;
	}
}
