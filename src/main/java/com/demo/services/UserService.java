package com.demo.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.expression.ParseException;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.demo.configuration.Access;
import com.demo.dao.ProfileDao;
import com.demo.dao.RolDao;
import com.demo.dao.UserDao;
import com.demo.dto.UserDto;
import com.demo.models.Profile;
import com.demo.models.Rol;
import com.demo.models.User;

@Service
public class UserService {
	@Autowired
    private ModelMapper modelMapper;
	@Autowired
	private UserDao userDao;
	@Autowired
	private ProfileDao profileDao;
	@Autowired
	private RolDao rolDao;
	@Autowired
	private PasswordEncoder passwordEncoder;
	/**
	 * Crud Service User
	 * 
	 **/
	public ResponseEntity<Object> createOne(UserDto userDto) {
		User f = userDao.findByEmail(userDto.getEmail());
		User g = userDao.findByUsername(userDto.getUsername());
		if(f == null && g == null) {
			Access acces = Access.VIEWER;//revisar para no dejar asi
			List<Rol> role = new ArrayList<>();
			User user = convertToEntity(userDto);
			Profile profile = new Profile();
			Rol rol = rolDao.findByAccess(acces).get(0);//primero
			role.add(rol);
			user.setIsActive(true);
			user.setPassword(passwordEncoder.encode(userDto.getPassword()));
			user.setProfile(profile);
			user.setRoles(role);
			userDao.save(user);
			profileDao.save(profile);
    		return ResponseEntity.created(null).build();//link
		}
		return ResponseEntity.badRequest().build();//conflit
	}
	public ResponseEntity<List<UserDto>> getAll() {
		List<User> users = userDao.findAll();
		List<UserDto> usersDto = users.stream().map(this::convertToDto).collect(Collectors.toList());
		return ResponseEntity.ok(usersDto);
	}
	public ResponseEntity<UserDto> getOne(Long idUser) {
		Optional<User> user = userDao.findById(idUser);
		if(user.isPresent())
			return ResponseEntity.ok(convertToDto(user.get()));
		else
			return ResponseEntity.notFound().build();
	}
	public ResponseEntity<Object> editOne(UserDto userDto,Long idUser) {
		Optional<User> user = userDao.findById(idUser);
		if(user.isPresent()) {
			User userEdited = user.get();
			if(userDto.getUsername() != null && !userDto.getUsername().isEmpty() && !userDto.getUsername().equals(userEdited.getUsername())) {
				User g = userDao.findByUsername(userDto.getUsername());
				if(g == null)
					userEdited.setUsername(userDto.getUsername());
			}/*else{
				return ResponseEntity.badRequest().build();
			}*/
			if(userDto.getEmail() != null && !userDto.getEmail().isEmpty() && !userDto.getEmail().equals(userEdited.getEmail())) {
				User f = userDao.findByEmail(userDto.getEmail());
				if(f == null)
					userEdited.setEmail(userDto.getEmail());
			}/*else{
				return ResponseEntity.badRequest().build();
			}*/
			if(userDto.getPassword()!= null && !userDto.getPassword().isEmpty())
				userEdited.setPassword(passwordEncoder.encode(userDto.getPassword()));
			userDao.save(userEdited);
			return ResponseEntity.ok(convertToDto(userEdited));
		}
		return ResponseEntity.notFound().build();
	}
	public ResponseEntity<Object> markDeleteOne(Long idUser) {
		Optional<User> user = userDao.findById(idUser);
		if(user.isPresent()){
			User userEdited = user.get();
			userEdited.setIsActive((userEdited.getIsActive()) ? false : true);
			userDao.save(userEdited);
			return  ResponseEntity.ok().build();
		}
		return ResponseEntity.notFound().build();
	}
	public ResponseEntity<Object> deleteOne(Long idUser) {
		Optional<User> user = userDao.findById(idUser);
		if(user.isPresent())
			if(!user.get().getIsActive()) {
				userDao.deleteById(idUser);
				return  ResponseEntity.ok().build();
			}
		return ResponseEntity.notFound().build();
	}
	/**
	 * Paginate
	 **/
	public ResponseEntity<Page<User>> getPagination(Pageable paging) {
    	Page<User> users =  userDao.findAll(paging);
		return ResponseEntity.ok(users);
	}
	/**
	 * Validate Credentials
	 * @param
	 * String username
	 * String password
	 **/
	public Boolean validateCredential(String username,String pasword) {	
		BCryptPasswordEncoder b = new BCryptPasswordEncoder();
		User u = userDao.findByUsername(username);
		if (u != null) {
			return (b.matches(pasword, u.getPassword()))? true : false;
		}
		return false;
	}
	/**
	 * Model Mapper 
	 **/
	private User convertToEntity(UserDto userDto) throws ParseException {
		User user = modelMapper.map(userDto, User.class);
	    return user;
	}
	private UserDto convertToDto(User user) {
		UserDto userDto = modelMapper.map(user, UserDto.class);
	    return userDto;
	}
}
